import Vue from "vue";
import App from "./App.vue";
const axios = require("axios");

Vue.config.productionTip = false;
var eventHub = new Vue();
global.eventHub = eventHub;

new Vue({
	render: (h) => h(App),
	data: {
		content: null,
	},
	mounted() {
		this.getPageContent();
	},
	methods: {
		async getPageContent() {
			const content = await axios.get("/api/getAllInfo");
			this.content = content.data.pages;
			eventHub.$emit("info-loaded");
		},
	},
}).$mount("#app");
