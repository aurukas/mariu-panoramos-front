/* webpackPrefetch: true */
const titulinis = () =>
  import(/* webpackChunkName: "titulinis" */ "./titulinis");
const namai = () => import(/* webpackChunkName: "namai" */ "./namai");

const routes = [
  {
    path: "/",
    name: "titulinis",
    component: titulinis,
  },
  {
    path: "*",
    name: "namai",
    component: namai,
  },
  // {
  //   path: "/aplinka",
  //   name: "aplinka",
  //   component: aplinka,
  // },
  // {
  //   path: "/gamta",
  //   name: "gamta",
  //   component: gamta,
  // },
  // {
  //   path: "/vandens-sportas",
  //   name: "vandens-sportas",
  //   component: vandens_sportas,
  // },
];
export default routes;
